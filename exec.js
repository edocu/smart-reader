const { spawn } = require('child_process');
const fs = require('fs')
const rc522 = require("rc522/build/Release/rc522");

console.log('Ready!!!');

rc522(createPayload)

function createPayload(data) {
	console.log('---------------------')
	console.log(data)
	console.log(typeof data)
	const payload = `GUI r\nDELAY 500\nSTRING ${data.valueOf()}`
	//fs.writeFileSync('/home/pi/payload.dd', payload) // works only with synchronous operation
	duckpi(data)
}

function duckpi(id) {
	const duckpiProcess = spawn('/home/pi/duckpivn.sh', [`/home/pi/payload_${id}.dd`], {encoding:'ucs2'});

	duckpiProcess.stdout.on('data', (data) => {
		console.log(`stdout: ${data}`);
	});

	duckpiProcess.stderr.on('data', (data) => {
        	console.log(`stderr: ${data}`);
        });

        duckpiProcess.on('close', (code) => {
        	console.log(`child process exited with code ${code}`);
        });
}
