#!/bin/bash

# Duckberry Pi
# Simple Duckyscript interpreter in Bash modified for the Raspberry Pi Zero.
# Based on droidducky and hid-gadget-test utility.
#
# Usage: duckpi.sh payload.dd
#
# Copyright (C) 2016 - Jeff L. (Renegade_R)
#
# Original droidducky script Copyright (C) 2015 - Andrej Budinčević <andrew@hotmail.rs>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

defdelay=0
kb="/dev/hidg0 keyboard"

last_cmd=""
last_string=""
line_num=0

function convert() 
{
	local kbcode=""

	if [ "$1" == " " ]
	then
		kbcode='space'
	elif [ "$1" == "!" ]
	then
		kbcode='left-shift 1'
	elif [ "$1" == "." ]
	then
		kbcode='period'
	elif [ "$1" == "\`" ]
	then
		kbcode='backquote'
	elif [ "$1" == "~" ]
	then
		kbcode='left-shift tilde'
	elif [ "$1" == "+" ]
	then
		kbcode='kp-plus'
	elif [ "$1" == "=" ]
	then
		kbcode='equal'
	elif [ "$1" == "_" ]
	then
		kbcode='left-shift minus'
	elif [ "$1" == "-" ]
	then
		kbcode='minus'
	elif [ "$1" == "\"" ]
	then
		kbcode='left-shift quote'
	elif [ "$1" == "'" ]
	then
		kbcode='quote'
	elif [ "$1" == ":" ]
	then
		kbcode='left-shift semicolon'
	elif [ "$1" == ";" ]
	then
		kbcode='semicolon'
	elif [ "$1" == "<" ]
	then
		kbcode='left-shift comma'
	elif [ "$1" == "," ]
	then
		kbcode='comma'
	elif [ "$1" == ">" ]
	then
		kbcode='left-shift period'
	elif [ "$1" == "?" ]
	then
		kbcode='left-shift slash'
	elif [ "$1" == "\\" ]
	then
		kbcode='backslash'
	elif [ "$1" == "|" ]
	then
		kbcode='left-shift backslash'
	elif [ "$1" == "/" ]
	then
		kbcode='slash'
	elif [ "$1" == "{" ]
	then
		kbcode='left-shift lbracket'
	elif [ "$1" == "}" ]
	then
		kbcode='left-shift rbracket'
	elif [ "$1" == "(" ]
	then
		kbcode='left-shift 9'
	elif [ "$1" == ")" ]
	then
		kbcode='left-shift 0'
	elif [ "$1" == "[" ]
	then
		kbcode='lbracket'
	elif [ "$1" == "]" ]
	then
		kbcode='rbracket'
	elif [ "$1" == "#" ]
	then
		kbcode='left-shift 3'
	elif [ "$1" == "@" ]
	then
		kbcode='left-shift 2'
	elif [ "$1" == "$" ]
	then
		kbcode='left-shift 4'
	elif [ "$1" == "%" ]
	then
		kbcode='left-shift 5'
	elif [ "$1" == "^" ]
	then
		kbcode='left-shift 6'
	elif [ "$1" == "&" ]
	then
		kbcode='left-shift 7'
	elif [ "$1" == "*" ]
	then
		kbcode='kp-multiply'
    elif [ "$1" == "Á" ]
    then
        kbcode='left-shift a 8'
    elif [ "$1" == "Đ" ]
    then
        kbcode='left-shift 0'
    elif [ "$1" == "ă" ]
    then
        kbcode='1'
    elif [ "$1" == "â" ]
    then
        kbcode='2'
    elif [ "$1" == "á" ]
    then
        kbcode='a 8'
    elif [ "$1" == "ắ" ]
    then
        kbcode='1 8'
    elif [ "$1" == "ấ" ]
    then
        kbcode='2 8'
    elif [ "$1" == "à" ]
    then
        kbcode='a 5'
    elif [ "$1" == "ằ" ]
    then
        kbcode='1 5'
    elif [ "$1" == "ầ" ]
    then
        kbcode='2 5'
    elif [ "$1" == "ả" ]
    then
        kbcode='a 6'
    elif [ "$1" == "ẳ" ]
    then
        kbcode='1 6'
    elif [ "$1" == "ẩ" ]
    then
        kbcode='2 6'
    elif [ "$1" == "ã" ]
    then
        kbcode='a 7'
    elif [ "$1" == "ẵ" ]
    then
        kbcode='1 7'
    elif [ "$1" == "ẫ" ]
    then
        kbcode='2 7'
    elif [ "$1" == "ạ" ]
    then
        kbcode='a 9'
    elif [ "$1" == "ặ" ]
    then
        kbcode='1 9'
    elif [ "$1" == "ậ" ]
    then
        kbcode='2 9'
    elif [ "$1" == "đ" ]
    then
        kbcode='0'
    elif [ "$1" == "₫" ]
    then
        kbcode='equal'
    elif [ "$1" == "ê" ]
    then
        kbcode='3'
    elif [ "$1" == "é" ]
    then
        kbcode='e 8'
    elif [ "$1" == "ế" ]
    then
        kbcode='3 8'
    elif [ "$1" == "è" ]
    then
        kbcode='e 5'
    elif [ "$1" == "ề" ]
    then
        kbcode='3 5'
    elif [ "$1" == "ẻ" ]
    then
        kbcode='e 6'
    elif [ "$1" == "ể" ]
    then
        kbcode='3 6'
    elif [ "$1" == "ẽ" ]
    then
        kbcode='e 7'
    elif [ "$1" == "ễ" ]
    then
        kbcode='3 7'
    elif [ "$1" == "ẹ" ]
    then
        kbcode='e 9'
    elif [ "$1" == "ệ" ]
    then
        kbcode='3 9'
    elif [ "$1" == "í" ]
    then
        kbcode='i 8'
    elif [ "$1" == "ì" ]
    then
        kbcode='i 5'
    elif [ "$1" == "ỉ" ]
    then
        kbcode='i 6'
    elif [ "$1" == "ĩ" ]
    then
        kbcode='i 7'
    elif [ "$1" == "ị" ]
    then
        kbcode='i 9'
    elif [ "$1" == "ô" ]
    then
        kbcode='4'
    elif [ "$1" == "ơ" ]
    then
        kbcode='rbracket'
    elif [ "$1" == "ó" ]
    then
        kbcode='o 8'
    elif [ "$1" == "ố" ]
    then
        kbcode='4 8'
    elif [ "$1" == "ớ" ]
    then
        kbcode='rbracket 8'
    elif [ "$1" == "ò" ]
    then
        kbcode='o 5'
    elif [ "$1" == "ồ" ]
    then
        kbcode='4 5'
    elif [ "$1" == "ờ" ]
    then
        kbcode='rbracket 5'
    elif [ "$1" == "ỏ" ]
    then
        kbcode='o 6'
    elif [ "$1" == "ổ" ]
    then
        kbcode='4 6'
    elif [ "$1" == "ở" ]
    then
        kbcode='rbracket 6'
    elif [ "$1" == "õ" ]
    then
        kbcode='o 7'
    elif [ "$1" == "ỗ" ]
    then
        kbcode='o 7'
    elif [ "$1" == "ỡ" ]
    then
        kbcode='rbracket 7'
    elif [ "$1" == "ọ" ]
    then
        kbcode='o 9'
    elif [ "$1" == "ộ" ]
    then
        kbcode='4 9'
    elif [ "$1" == "ợ" ]
    then
        kbcode='rbracket 9'
    elif [ "$1" == "ư" ]
    then
        kbcode='lbracket'
    elif [ "$1" == "ú" ]
    then
        kbcode='u 8'
    elif [ "$1" == "ứ" ]
    then
        kbcode='lbracket 8'
    elif [ "$1" == "ù" ]
    then
        kbcode='u 5'
    elif [ "$1" == "ừ" ]
    then
        kbcode='lbracket 5'
    elif [ "$1" == "ủ" ]
    then
        kbcode='u 6'
    elif [ "$1" == "ử" ]
    then
        kbcode='lbracket 6'
    elif [ "$1" == "ũ" ]
    then
        kbcode='u 7'
    elif [ "$1" == "ữ" ]
    then
        kbcode='lbracket 7'
    elif [ "$1" == "ụ" ]
    then
        kbcode='u 9'
    elif [ "$1" == "ự" ]
    then
        kbcode='lbracket 9'
    elif [ "$1" == "ý" ]
    then
        kbcode='y 8'
    elif [ "$1" == "ỳ" ]
    then
        kbcode='y 5'
    elif [ "$1" == "ỷ" ]
    then
        kbcode='y 6'
    elif [ "$1" == "ỹ" ]
    then
        kbcode='y 7'
    elif [ "$1" == "ỵ" ]
    then
        kbcode='y 9'
	elif [ "$1" == "Ă" ]
	then
		kbcode='left-shift 1'
	elif [ "$1" == "ä" ]
	then
		kbcode='left-shift 2'
	elif [ "$1" == "Á" ]
	then
		kbcode='left-shift a 8'
	elif [ "$1" == "Ắ" ]
	then
		kbcode='left-shift 1 8'
	elif [ "$1" == "Ấ" ]
	then
		kbcode='left-shift 2 8'
	elif [ "$1" == "À" ]
	then
		kbcode='left-shift a 5'
	elif [ "$1" == "Ằ" ]
	then
		kbcode='left-shift 1 5'
	elif [ "$1" == "Ầ" ]
	then
		kbcode='left-shift 2 5'
	elif [ "$1" == "Ả" ]
	then
		kbcode='left-shift a 6'
	elif [ "$1" == "Ẳ" ]
	then
		kbcode='left-shift 1 6'
	elif [ "$1" == "Ẩ" ]
	then
		kbcode='left-shift 2 6'
	elif [ "$1" == "Ã" ]
	then
		kbcode='left-shift a 7'
	elif [ "$1" == "Ẵ" ]
	then
		kbcode='left-shift 1 7'
	elif [ "$1" == "Ẫ" ]
	then
		kbcode='left-shift 2 7'
	elif [ "$1" == "Ạ" ]
	then
		kbcode='left-shift a 9'
	elif [ "$1" == "Ặ" ]
	then
		kbcode='left-shift 1 9'
	elif [ "$1" == "Ậ" ]
	then
		kbcode='left-shift 2 9'
	elif [ "$1" == "Đ" ]
	then
		kbcode='left-shift 0'
	elif [ "$1" == "Ê" ]
	then
		kbcode='left-shift 3'
	elif [ "$1" == "É" ]
	then
		kbcode='left-shift e 8'
	elif [ "$1" == "Ế" ]
	then
		kbcode='left-shift 3 8'
	elif [ "$1" == "È" ]
	then
		kbcode='left-shift e 5'
	elif [ "$1" == "Ề" ]
	then
		kbcode='left-shift 3 5'
	elif [ "$1" == "Ẻ" ]
	then
		kbcode='left-shift e 6'
	elif [ "$1" == "Ể" ]
	then
		kbcode='left-shift 3 6'
	elif [ "$1" == "Ẽ" ]
	then
		kbcode='left-shift e 7'
	elif [ "$1" == "Ễ" ]
	then
		kbcode='left-shift 3 7'
	elif [ "$1" == "Ẹ" ]
	then
		kbcode='left-shift e 9'
	elif [ "$1" == "Ệ" ]
	then
		kbcode='left-shift 3 9'
	elif [ "$1" == "Í" ]
	then
		kbcode='left-shift i 8'
	elif [ "$1" == "Ì" ]
	then
		kbcode='left-shift i 5'
	elif [ "$1" == "Ỉ" ]
	then
		kbcode='left-shift i 6'
	elif [ "$1" == "Ĩ" ]
	then
		kbcode='left-shift i 7'
	elif [ "$1" == "Ị" ]
	then
		kbcode='left-shift i 9'
	elif [ "$1" == "Ô" ]
	then
		kbcode='left-shift 4'
	elif [ "$1" == "Ơ" ]
	then
		kbcode='left-shift rbracket'
	elif [ "$1" == "Ó" ]
	then
		kbcode='left-shift o 8'
	elif [ "$1" == "Ố" ]
	then
		kbcode='left-shift 4 8'
	elif [ "$1" == "Ớ" ]
	then
		kbcode='left-shift rbracket 8'
	elif [ "$1" == "Ò" ]
	then
		kbcode='left-shift o 5'
	elif [ "$1" == "Ồ" ]
	then
		kbcode='left-shift 4 5'
	elif [ "$1" == "Ờ" ]
	then
		kbcode='left-shift rbracket 5'
	elif [ "$1" == "Ỏ" ]
	then
		kbcode='left-shift o 6'
	elif [ "$1" == "Ổ" ]
	then
		kbcode='left-shift 4 6'
	elif [ "$1" == "Ở" ]
	then
		kbcode='left-shift rbracket 6'
	elif [ "$1" == "Õ" ]
	then
		kbcode='left-shift o 7'
	elif [ "$1" == "Ỗ" ]
	then
		kbcode='left-shift ô 7'
	elif [ "$1" == "Ỡ" ]
	then
		kbcode='left-shift rbracket 7'
	elif [ "$1" == "Ọ" ]
	then
		kbcode='left-shift o 9'
	elif [ "$1" == "Ộ" ]
	then
		kbcode='left-shift 4 9'
	elif [ "$1" == "Ợ" ]
	then
		kbcode='left-shift rbracket 9'
	elif [ "$1" == "Ư" ]
	then
		kbcode='left-shift lbracket'
	elif [ "$1" == "Ú" ]
	then
		kbcode='left-shift u 8'
	elif [ "$1" == "Ứ" ]
	then
		kbcode='left-shift lbracket 8'
	elif [ "$1" == "Ù" ]
	then
		kbcode='left-shift u 5'
	elif [ "$1" == "Ừ" ]
	then
		kbcode='left-shift lbracket 5'
	elif [ "$1" == "Ủ" ]
	then
		kbcode='left-shift u 6'
	elif [ "$1" == "Ử" ]
	then
		kbcode='left-shift lbracket 6'
	elif [ "$1" == "Ũ" ]
	then
		kbcode='left-shift u 7'
	elif [ "$1" == "Ữ" ]
	then
		kbcode='left-shift lbracket 7'
	elif [ "$1" == "Ụ" ]
	then
		kbcode='left-shift u 9'
	elif [ "$1" == "Ự" ]
	then
		kbcode='left-shift lbracket 9'
	elif [ "$1" == "Ý" ]
	then
		kbcode='left-shift y 8'
	elif [ "$1" == "Ỳ" ]
	then
		kbcode='left-shift y 5'
	elif [ "$1" == "Ỷ" ]
	then
		kbcode='left-shift y 6'
	elif [ "$1" == "Ỹ" ]
	then
		kbcode='left-shift y 7'
	elif [ "$1" == "Ỵ" ]
	then
		kbcode='left-shift y 9'

	elif [ "$1" == "0" ]
	then
		kbcode='kp-0'
	elif [ "$1" == "1" ]
	then
		kbcode='kp-1'
	elif [ "$1" == "2" ]
	then
		kbcode='kp-2'
	elif [ "$1" == "3" ]
	then
		kbcode='kp-3'
	elif [ "$1" == "4" ]
	then
		kbcode='kp-4'
	elif [ "$1" == "5" ]
	then
		kbcode='kp-5'
	elif [ "$1" == "6" ]
	then
		kbcode='kp-6'
	elif [ "$1" == "7" ]
	then
		kbcode='kp-7'
	elif [ "$1" == "8" ]
	then
		kbcode='kp-8'
	elif [ "$1" == "9" ]
	then
		kbcode='kp-9'

	else
		case $1 in
		[[:upper:]])
			tmp=$1
			kbcode="left-shift ${tmp,,}"
			;;
		*)
			kbcode="$1"
			;;
		esac
	fi

	echo "$kbcode"
}

while IFS='' read -r line || [[ -n "$line" ]]; do
	((line_num++))
	read -r cmd info <<< "$line"
	if [ "$cmd" == "STRING" ] 
	then
		last_string="$info"
		last_cmd="$cmd"

		for ((  i=0; i<${#info}; i++  )); do
			kbcode=$(convert "${info:$i:1}")
			echo "kbcode: $kbcode"
			if [ "$kbcode" != "" ]
			then
				echo "$kbcode" | /home/pi/hid-gadget-test $kb > /dev/null
			fi
		done
	elif [ "$cmd" == "ENTER" ] 
	then
		last_cmd="enter"
		echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null
	
	elif [ "$cmd" == "DELAY" ] 
	then
		last_cmd="UNS"
		((info = info*1000))
		/home/pi/usleep $info

	elif [ "$cmd" == "WINDOWS" -o "$cmd" == "GUI" ] 
	then
		last_cmd="left-meta ${info,,}"
		echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

	elif [ "$cmd" == "MENU" -o "$cmd" == "APP" ] 
	then
		last_cmd="left-shift f10"
		echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

	elif [ "$cmd" == "DOWNARROW" -o "$cmd" == "DOWN" ] 
	then
		last_cmd="down"
		echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

	elif [ "$cmd" == "LEFTARROW" -o "$cmd" == "LEFT" ] 
	then
		last_cmd="left"
		echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

	elif [ "$cmd" == "RIGHTARROW" -o "$cmd" == "RIGHT" ] 
	then
		last_cmd="right"
		echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

	elif [ "$cmd" == "UPARROW" -o "$cmd" == "UP" ] 
	then
		last_cmd="up"
		echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

	elif [ "$cmd" == "DEFAULT_DELAY" -o "$cmd" == "DEFAULTDELAY" ] 
	then
		last_cmd="UNS"
		((defdelay = info*1000))

	elif [ "$cmd" == "BREAK" -o "$cmd" == "PAUSE" ] 
	then
		last_cmd="pause"
		echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

	elif [ "$cmd" == "ESC" -o "$cmd" == "ESCAPE" ] 
	then
		last_cmd="escape"
		echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

	elif [ "$cmd" == "PRINTSCREEN" ] 
	then
		last_cmd="print"
		echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

	elif [ "$cmd" == "CAPSLOCK" -o "$cmd" == "DELETE" -o "$cmd" == "END" -o "$cmd" == "HOME" -o "$cmd" == "INSERT" -o "$cmd" == "NUMLOCK" -o "$cmd" == "PAGEUP" -o "$cmd" == "PAGEDOWN" -o "$cmd" == "SCROLLLOCK" -o "$cmd" == "SPACE" -o "$cmd" == "TAB" \
	-o "$cmd" == "F1" -o "$cmd" == "F2" -o "$cmd" == "F3" -o "$cmd" == "F4" -o "$cmd" == "F5" -o "$cmd" == "F6" -o "$cmd" == "F7" -o "$cmd" == "F8" -o "$cmd" == "F9" -o "$cmd" == "F10" -o "$cmd" == "F11" -o "$cmd" == "F12" ] 
	then
		last_cmd="${cmd,,}"
		echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

	elif [ "$cmd" == "REM" ] 
	then
		echo "$info"

	elif [ "$cmd" == "SHIFT" ] 
	then
		if [ "$info" == "DELETE" -o "$info" == "END" -o "$info" == "HOME" -o "$info" == "INSERT" -o "$info" == "PAGEUP" -o "$info" == "PAGEDOWN" -o "$info" == "SPACE" -o "$info" == "TAB" ] 
		then
			last_cmd="left-shift ${info,,}"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == *"WINDOWS"* -o "$info" == *"GUI"* ] 
		then
			read -r gui char <<< "$info"
			last_cmd="left-shift left-meta ${char,,}"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "DOWNARROW" -o "$info" == "DOWN" ] 
		then
			last_cmd="left-shift down"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "LEFTARROW" -o "$info" == "LEFT" ] 
		then
			last_cmd="left-shift left"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "RIGHTARROW" -o "$info" == "RIGHT" ] 
		then
			last_cmd="left-shift right"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "UPARROW" -o "$info" == "UP" ] 
		then
			last_cmd="left-shift up"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "F1" -o "$info" == "F2" -o "$info" == "F3" -o "$info" == "F4" -o "$info" == "F5" -o "$info" == "F6" -o "$info" == "F7" -o "$info" == "F8" -o "$info" == "F9" -o "$info" == "F10" -o "$info" == "F11" -o "$info" == "F12" ] 
		then
			last_cmd="left-shift ${info,,}"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null
		else
			echo "($line_num) Parse error: Disallowed $cmd $info"
		fi

	elif [ "$cmd" == "CONTROL" -o "$cmd" == "CTRL" ] 
	then
		if [ "$info" == "BREAK" -o "$info" == "PAUSE" ] 
		then
			last_cmd="left-ctrl pause"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "F1" -o "$info" == "F2" -o "$info" == "F3" -o "$info" == "F4" -o "$info" == "F5" -o "$info" == "F6" -o "$info" == "F7" -o "$info" == "F8" -o "$info" == "F9" -o "$info" == "F10" -o "$info" == "F11" -o "$info" == "F12" ] 
		then
			last_cmd="left-ctrl ${cmd,,}"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "ESC" -o "$info" == "ESCAPE" ] 
		then
			last_cmd="left-ctrl escape"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "" ]
		then
			last_cmd="left-ctrl"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		else 
			last_cmd="left-ctrl ${info,,}"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null
		fi

	elif [ "$cmd" == "ALT" ] 
	then
		if [ "$info" == "END" -o "$info" == "SPACE" -o "$info" == "TAB" \
		-o "$info" == "F1" -o "$info" == "F2" -o "$info" == "F3" -o "$info" == "F4" -o "$info" == "F5" -o "$info" == "F6" -o "$info" == "F7" -o "$info" == "F8" -o "$info" == "F9" -o "$info" == "F10" -o "$info" == "F11" -o "$info" == "F12" ] 
		then
			last_cmd="left-alt ${info,,}"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "ESC" -o "$info" == "ESCAPE" ] 
		then
			last_cmd="left-alt escape"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "" ]
		then
			last_cmd="left-alt"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		else 
			last_cmd="left-alt ${info,,}"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null
		fi

	elif [ "$cmd" == "ALT-SHIFT" ] 
	then
		last_cmd="left-shift left-alt"
		echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

	elif [ "$cmd" == "CTRL-ALT" ] 
	then
		if [ "$info" == "BREAK" -o "$info" == "PAUSE" ] 
		then
			last_cmd="left-ctrl left-alt pause"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "END" -o "$info" == "SPACE" -o "$info" == "TAB" -o "$info" == "DELETE" -o "$info" == "F1" -o "$info" == "F2" -o "$info" == "F3" -o "$info" == "F4" -o "$info" == "F5" -o "$info" == "F6" -o "$info" == "F7" -o "$info" == "F8" -o "$info" == "F9" -o "$info" == "F10" -o "$info" == "F11" -o "$info" == "F12" ] 
		then
			last_cmd="left-ctrl left-alt ${cmd,,}"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "ESC" -o "$info" == "ESCAPE" ] 
		then
			last_cmd="left-ctrl left-alt escape"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "" ]
		then
			last_cmd="left-ctrl left-alt"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		else 
			last_cmd="left-ctrl left-alt ${info,,}"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null
		fi

	elif [ "$cmd" == "CTRL-SHIFT" ] 
	then
		if [ "$info" == "BREAK" -o "$info" == "PAUSE" ] 
		then
			last_cmd="left-ctrl left-shift pause"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "END" -o "$info" == "SPACE" -o "$info" == "TAB" -o "$info" == "DELETE" -o "$info" == "F1" -o "$info" == "F2" -o "$info" == "F3" -o "$info" == "F4" -o "$info" == "F5" -o "$info" == "F6" -o "$info" == "F7" -o "$info" == "F8" -o "$info" == "F9" -o "$info" == "F10" -o "$info" == "F11" -o "$info" == "F12" ] 
		then
			last_cmd="left-ctrl left-shift ${cmd,,}"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "ESC" -o "$info" == "ESCAPE" ] 
		then
			last_cmd="left-ctrl left-shift escape"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		elif [ "$info" == "" ]
		then
			last_cmd="left-ctrl left-shift"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null

		else 
			last_cmd="left-ctrl left-shift ${info,,}"
			echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null
		fi

	elif [ "$cmd" == "REPEAT" ] 
	then
		if [ "$last_cmd" == "UNS" -o "$last_cmd" == "" ]
		then
			echo "($line_num) Parse error: Using REPEAT with DELAY, DEFAULTDELAY or BLANK is not allowed."
		else
			for ((  i=0; i<$info; i++  )); do
				if [ "$last_cmd" == "STRING" ] 
				then
					for ((  j=0; j<${#last_string}; j++  )); do
						kbcode=$(convert "${last_string:$j:1}")

						if [ "$kbcode" != "" ]
						then
							echo "$kbcode" | /home/pi/hid-gadget-test $kb > /dev/null
						fi
					done
				else
					echo "$last_cmd" | /home/pi/hid-gadget-test $kb > /dev/null
				fi
				/home/pi/usleep $defdelay
			done
		fi

	elif [ "$cmd" != "" ] 
	then
		echo "($line_num) Parse error: Unexpected $cmd."
	fi
	/home/pi/usleep $defdelay
done < "$1"